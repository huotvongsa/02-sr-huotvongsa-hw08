import './App.css';
import React,{Component} from 'react';
import MyForm from './components/MyForm'
import MyTable from './components/MyTable';
import {Container,Row,Col} from 'react-bootstrap'


class App extends Component{
  render(){
    return (
      <div>
        <Container>
          <Row>
            <Col sm={4}>
              <MyForm></MyForm>
            </Col>
            <Col sm={8} >
              <MyTable></MyTable>
            </Col>
            
          </Row>    
        </Container>
      </div>
    );
  }
}
export default App;