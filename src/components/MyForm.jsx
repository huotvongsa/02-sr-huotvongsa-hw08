import React, { Component } from "react";
import { Form, Button, Image } from "react-bootstrap";

export default class MyForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username:"",
      usernameErr:"",
      email: "",
      emailErr:"",
      password: "",
      passwordErr:"",
      gender: "male"
      
    };
  }
  onHandleText = (e) => {
    console.log("e:", e.target);
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);

        if (e.target.name === "username") {
            let pattern =
              /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,24}$/;
            let result = pattern.test(this.state.username.trim());
  
            if (result) {
              this.setState({
                usernameErr: "",
              });
            } else if (this.state.username === "") {
              this.setState({
                usernameErr: "Username cannot be empty",
              });
            } else {
              this.setState({
                usernameErr: "Username is invalip",
              });
            }
          }

        if (e.target.name === "email") {
          let pattern =
            /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
          let result = pattern.test(this.state.email.trim());

          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty",
            });
          } else {
            this.setState({
              emailErr: "Email is invalip",
            });
          }
        }

        if (e.target.name === "password") {
          let pattern =
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
          let result = pattern.test(this.state.password.trim());

          if (result) {
            this.setState({
              passwordErr: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordErr: "password cannot be empty",
            });
          } else {
            this.setState({
              passwordErr: "password is invalid",
            });
          }
        }
      }
    );
  };
  validateBtn = () => {
    if (
      this.state.emailErr === "" &&
      this.state.passwordErr === "" &&
      this.state.email !== "" &&
      this.state.password !== ""
    ) {
      return false;
    } else {
      return true;
    }
  };
  render() {
    return (
      <div>
        <Form>
          <div style={{ textAlign: "center" }}>
            <Image
              width="200px"
              height="200px"
              src="1.jpg"
              roundedCircle
            ></Image>
            <h2>Create Account</h2>
          </div>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control
              onChange={this.onHandleText}
              name="username"
              type="text"
              placeholder="username"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.usernameErr}</p>
            </Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Label>Gender</Form.Label> <br />
            <Form.Check
              custom
              inline
              label="male"
              type="radio"
              id="male"
              name="gender"
              defaultChecked ={this.state.gender==="male"?true:false}
            />
            <Form.Check
              custom
              inline
              label="female"
              type="radio"
              id="female"
              name="gender"
              defaultChecked ={this.state.gender==="female"?true:false}
            />
          </Form.Group>

          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              onChange={this.onHandleText}
              name="email"
              type="email"
              placeholder="Enter email"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.emailErr}</p>
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              onChange={this.onHandleText}
              name="password"
              type="password"
              placeholder="Password"
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.passwordErr}</p>
            </Form.Text>
          </Form.Group>
          <Button disabled={this.validateBtn()} variant="primary" type="submit">
            Save
          </Button>
        </Form>
      </div>
    );
  }
}
